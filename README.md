# Checkmk free (free enterprise version for max 25 hosts)

[Based on this](https://github.com/tribe29/checkmk/tree/master/docker) (slightly adjusted) Containerfile to build and run the Checkmk enterprise free version in a Debian or Rocky Linux based container.
This repo contains two Containerfile, one based on Debian buster and the other based on Rocky Linux 8.
In the examples below the Rocky Linux 8 version is used.
The containers are build and tested, pushed to this [Quay.io registry](https://quay.io/repository/juliu_s/checkmk-enterprise-demo) with a GitLab pipeline.

*Note:* Checkmk renamed the enterprise demo version to free. Since this repo and Quay registry was already set up the names are mixed up.

# Build yourself

```shell-session
git clone https://gitlab.com/juliu-s/container-checkmk-enterprise-demo
cd container-checkmk-enterprise-demo
podman build -f Containerfile.rocky8 -t foo/checkmk-enterprise-free:2.0.0p19-rocky8 .
```

Or build it yourself with a other version as build arg

```shell-session
podman build -f Containerfile.rocky8 -t foo/checkmk-enterprise-free:2.0.0p20-rocky8 --build-arg CMK_VERSION=2.0.0p20 .
```

# Pull a version from Quay.io

```shell-session
podman pull quay.io/juliu_s/checkmk-enterprise-demo:2.0.0p19-rocky8
```

## Run it

Self build version:

```shell-session
podman run -d \
  --name monitoring \
  -p 5000:5000 \
  --ulimit nofile=1024 \
  -v "/datadir/on/containerhost/checkmk/:/omd/sites" \
  -v /etc/localtime:/etc/localtime \
  -e CMK_PASSWORD="cmkadmin" \
  --restart always \
  foo/checkmk-enterprise-free:2.0.0p20-rocky8
```

Quay.io version:

```shell-session
podman run -d \
  --name monitoring \
  -p 5000:5000 \
  --ulimit nofile=1024 \
  -v "/datadir/on/containerhost/checkmk/:/omd/sites" \
  -v /etc/localtime:/etc/localtime \
  -e CMK_PASSWORD="cmkadmin" \
  --restart always \
  quay.io/juliu_s/checkmk-enterprise-demo:2.0.0p19-rocky8
```

The checkmk web interface should now be reachable via http://localhost:5000/cmk/check_mk/

## Test it

Check monitoring core

```shell-session
podman exec -it monitoring su - cmk -c "omd config show"
```

## Check the container logs

```shell-session
podman logs -f monitoring
```

# Links

* [Checkmk website](https://checkmk.com/)
* [Checkmk container docs](https://docs.checkmk.com/latest/en/introduction_docker.html)
* [Create own Checkmk container](https://checkmk.com/cms_managing_docker.html#Creating%20your%20own%20container-images)
* [Checkmk RAW edition on Dockerhub](https://hub.docker.com/r/checkmk/check-mk-raw/)
* [Checkmk editions](https://checkmk.com/editions.html)

